# Create an IAM role
resource "aws_iam_role" "s3_role_tf" {
  name               = "s3_role_tf"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json

  tags = {
    Name = "s3_role_tf"
  }
}

# Allow the IAM role to be assumed by EC2 instances
data "aws_iam_policy_document" "assume_role" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

# Attach the s3 full access permissions to the IAM role
resource "aws_iam_role_policy" "s3_full_access_tf" {
  name   = "s3_full_access_tf"  
  role   = aws_iam_role.s3_role_tf.id
  policy = data.aws_iam_policy_document.s3_full_access.json
}

# Create an IAM policy that grants s3 full access permissions
data "aws_iam_policy_document" "s3_full_access" {
  statement {
    effect    = "Allow"
    actions   = ["s3:*"]
    resources = ["*"]
  }
}

# Create an instance profile with the IAM role attached
resource "aws_iam_instance_profile" "my_profile_tf" {
  name = "my_profile_tf"  
  role = aws_iam_role.s3_role_tf.name

  tags = {
    Name = "my_profile_tf"
  }
}