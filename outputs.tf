output "s3_bucket_id" {
    description = "Name of the bucket"
    value       = aws_s3_bucket.my_s3.id
}

output "instance_public_ip" {
  description = "Public IP address of the EC2 instance"
  value       = aws_instance.app_server.public_ip
}  