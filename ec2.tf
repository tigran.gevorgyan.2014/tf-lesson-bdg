resource "aws_instance" "app_server" {
  ami                    = var.ec2_ami
  instance_type          = var.ec2_instance_type
  subnet_id              = var.public_subnet_id
  availability_zone      = var.my_az 
  vpc_security_group_ids = [aws_security_group.custom_sg.id]
  user_data              = file("script.sh") 
  iam_instance_profile = aws_iam_instance_profile.my_profile_tf.name    

  tags = {
    Name = "ExampleAppServerInstance"
  }
}

resource "aws_ebs_volume" "vol1" {
  availability_zone = "us-east-1a"
  size              = 10
  type              = "gp3"

  tags = {
    Name = "appserver_vol1"
  }
}   

resource "aws_volume_attachment" "vol1" {
  device_name = "/dev/sdb"
  volume_id   = aws_ebs_volume.vol1.id
  instance_id = aws_instance.app_server.id
}