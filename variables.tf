# Here stored variables for infrastructure.

variable "ec2_instance_type" {
  type = string
  description = "The instunce type of ec2"
}

variable "ec2_ami" {
  type = string
  description = "The ami of app server"
}

variable "public_subnet_id" {
  type = string
  description = "Public subnet id"
}

variable "my_az" {
  type = string
  description = "AZ for this example"
}

variable "default_vpc" {
  type = string
  description = "default vpc"
}