resource "aws_s3_bucket" "my_s3" {
  bucket = "lesson-tf-bdg"

  tags = {
    Name        = "lesson-tf-bdg"
  }
}

resource "aws_s3_bucket_acl" "s3_access" {
  bucket = aws_s3_bucket.my_s3.id
  acl    = "private"
}

resource "aws_s3_bucket_versioning" "versioning_s3" {
  bucket = aws_s3_bucket.my_s3.id
  versioning_configuration {
    status = "Disabled"
  }
}